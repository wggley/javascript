/**
 * Chart Generation API
 * Generates a Pie or a Line chart based on user input
 * @author  original Piktochart
 * @author  refactor Wallace Goulart Gaudie Ley
 */
var chartGenerator = chartGenerator || (function() {
	//Variables for All Chart
	var canvasWidth = 800;
	var canvasHeight = 450;
	var chartId = 1;
	var chartWrapper = document.getElementById('chartWrapper');
	var chartCanvas;
	var resizeX = canvasWidth - 150;
	var resizeY = 10;

	//Variables for PieChart
	var chartFromX = 250;
	var chartFromY = 220;
	var angleFrom, angleTo;
	var angleFromValue = 0;
	var angleToValue = 360;
	var chartA = 150;
	var chartArcFlag = 0;
	var chartD;
	var inputChartA = document.getElementById('inputPieR');

	//Variables for LineChart
	var lineFromX = 50;
	var lineFromY = 50;
	var lineWidth = 400;
	var lineHeight = 300;
	var lineHighestValue = 10;
	var inputHighestValue = document.getElementById('inputHighestValue');
	var inputRandomColor = document.getElementById('inputRandomColor');

	//Variables for Statistic
	var inputChartTitle = document.getElementById('inputChartTitle');
	var titleX = 30;
	var titleY = 30;
	var downloadX = canvasWidth - 100;
	var downloadY = 10;
	var deleteX = canvasWidth - 50;
	var deleteY = 10;
	var statisticX = 510;
	var statisticY = 75;
	var statLineHeight = 30;

	//Variables for Chart Data
	var chartData = {};
	var totalData = 0;
	var highestData = 0;
	var lastIdData = 0;
	var dataErrorMsg = "";
	var dataError = document.getElementById('errorMsg');
	//Icons formula from : http://iconmonstr.com/
	var icons = {
		men: 'M407.448,360.474c-59.036-13.617-113.989-25.541-87.375-75.717 c81.01-152.729,21.473-234.406-64.072-234.406c-87.231,0-145.303,84.812-64.072,234.406c27.412,50.482-29.608,62.393-87.375,75.717 c-59.012,13.609-54.473,44.723-54.473,101.176h411.838C461.919,405.196,466.458,374.083,407.448,360.474z',
		women: 'M407.447,380.817c-50.919-11.622-112.919-41.622-94.652-80.682c11.732,19.236,48.084,30.361,76.4,13.736 c-81.506-20.57,40.066-161.795-66.458-240.959c-38.877-29.041-95.209-29.373-133.474,0 c-106.524,79.164,15.048,220.389-66.458,240.959c28.316,16.625,64.723,5,76.4-13.736c18.268,39.064-43.786,69.072-94.652,80.682 c-59.041,13.475-54.473,52.025-54.473,80.176h411.838C461.919,432.843,466.489,394.294,407.447,380.817z',
		christmas: 'M392.677,400.564L430.553,462H81.447l37.876-61.436H392.677z M176.286,375.613 C130.607,348.403,100,298.531,100,241.502c0-86.156,69.844-156,156-156s156,69.844,156,156c0,57.029-30.607,106.901-76.286,134.111 h56.963c33.905-34.548,54.826-81.881,54.826-134.111C447.503,135.738,361.764,50,256,50C150.235,50,64.497,135.738,64.497,241.502 c0,52.23,20.921,99.563,54.826,134.111H176.286z M313.644,171.919c2.583,0,4.678-2.095,4.678-4.679c0-2.583-2.095-4.678-4.678-4.678 c-2.585,0-4.678,2.095-4.678,4.678C308.966,169.824,311.059,171.919,313.644,171.919z M325.807,219.513 c3.442,0,6.236-2.793,6.236-6.237c0-3.445-2.794-6.237-6.236-6.237c-3.445,0-6.237,2.792-6.237,6.237 C319.569,216.72,322.361,219.513,325.807,219.513z M352.314,254.856c5.167,0,9.356-4.188,9.356-9.355s-4.189-9.355-9.356-9.355 s-9.355,4.188-9.355,9.355S347.147,254.856,352.314,254.856z M347.637,190.631c2.583,0,4.678-2.096,4.678-4.679 c0-2.582-2.095-4.678-4.678-4.678s-4.678,2.096-4.678,4.678C342.959,188.535,345.054,190.631,347.637,190.631z M186.396,181.274 c5.168,0,9.357-4.189,9.357-9.355c0-5.167-4.189-9.356-9.357-9.356c-5.166,0-9.356,4.189-9.356,9.356 C177.039,177.085,181.229,181.274,186.396,181.274z M242.792,158.702l13.14-7.028l13.14,7.028l-2.623-14.669l10.743-10.326 l-14.761-2.039l-6.499-13.408l-6.501,13.408l-14.761,2.039l10.744,10.326L242.792,158.702z M299.572,280.737l32.132,42.552H180.158 l32.131-42.552h-21l26.94-37.455H202.94l52.991-82.233l52.989,82.233h-15.289l26.941,37.455H299.572z M284.473,257.639 l-20.046-29.95h16.239l-24.734-38.012l-24.736,38.012h16.239l-9.068,13.549C260.297,243.234,260.312,267.229,284.473,257.639z M244.08,375.237h23.702v-41.415H244.08V375.237z M155.008,189.175c-2.583,0-4.679,2.095-4.679,4.679 c0,2.582,2.096,4.678,4.679,4.678s4.678-2.096,4.678-4.678C159.686,191.27,157.591,189.175,155.008,189.175z M189.515,218.681 c0-3.444-2.792-6.237-6.237-6.237c-3.444,0-6.238,2.793-6.238,6.237s2.794,6.237,6.238,6.237 C186.723,224.918,189.515,222.125,189.515,218.681z M164.362,243.647c-2.582,0-4.677,2.095-4.677,4.679 c0,2.583,2.095,4.678,4.677,4.678c2.584,0,4.679-2.095,4.679-4.678C169.041,245.742,166.946,243.647,164.362,243.647z',
		star: 'M298.582,290.521l10.62,59.391L256,321.458l-53.202,28.453l10.62-59.391l-43.501-41.808 l59.766-8.252L256,186.17l26.317,54.291l59.767,8.252L298.582,290.521z M357.9,309.794l25.414,142.124L256,383.828l-127.315,68.09 L154.1,309.794L50,209.749l143.022-19.748L256,60.082l62.979,129.919L462,209.749L357.9,309.794z M342.89,396.277l-17.345-96.996 l71.046-68.279l-97.609-13.478L256,128.857l-42.981,88.667l-97.61,13.478l71.047,68.279l-17.345,96.996L256,349.809L342.89,396.277z'
	};
	var inputForm = document.getElementById('inputForm');
	var input_count = inputForm.rows.length;

	/* Public functions */
	var publicFunctions = {
		addData: addData,
		generateChart: generateChart,
		deleteData: deleteData,
		deleteChart: deleteChart,
		generateRandomColor: generateRandomColor
	}

	/** Methods **/

	/**
	 * Initialize page on Load
	 */
	window.onload = function() {

		// Initialize option
		initializeLineChartOption();
		initializePieChartOption();

		// Add 4 input form at beginning
		for (var i = 0; i < 4; i++) {
			addData();
		};

		//Event listener for error message
		dataError.addEventListener('click', function() {
			this.setAttribute('style', 'display:none');
		});
	};

	/**
	 * Calculate object size based on properties
	 * @param  {object} obj object to calculate size
	 * @return {integer}     size of object
	 */
	Object.size = function(obj) {
		var size = 0,
			key;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) {
				size++;
			}
		}
		return size;
	};

	/* LIST OF FUNCTIONS THAT MANIPULATES DATA INPUT */

	/**
	 * Add Data to Input Form
	 */
	function addData() {
		lastIdData++;
		var new_row = "";
		var input_count = lastIdData;
		var new_row_id = "inputData" + input_count;

		var input_name_id = "inputName" + input_count;
		var input_name_value = "Data " + input_count;

		var input_icon_id = "inputIcon" + input_count;
		var input_icon_value = "Data " + input_count;

		var input_color_id = "inputColor" + input_count;
		var input_color_preview_id = "inputColorPreview" + input_count;
		var input_color_value = getRandomColor();

		var input_value_id = "inputValue" + input_count;
		var input_value_value = Math.round(Math.random() * 9) + 1;

		//column delete row
		//Append the input to row
		var tr = createElementTag(inputForm, 'tr', {
			id: new_row_id
		});

		var td = createElementTag(tr, 'td');

		createElementTag(td, 'button', {
			onclick: 'deleteData(this);'
		}, '[X]');

		td.innerHTML += 'Data' + input_count;

		//column Name
		var td = createElementTag(tr, 'td', undefined, 'Name :');

		createElementTag(td, 'input', {
			type: 'text',
			size: 10,
			maxlength: 10,
			id: input_name_id,
			value: input_name_value
		});

		//column icon
		var td = createElementTag(tr, 'td');
		td.textContent = "Icon :";

		var select = createElementTag(td, 'select', {
			id: input_icon_id
		});

		for (icon in icons) {
			createElementTag(select, 'option', {
				value: icon
			}, icon);
		}

		//column preview color
		var td = createElementTag(tr, 'td');
		var div = createElementTag(td, 'div', {
			class: 'wrap'
		}, 'Color :');
		createElementTag(div, 'div', {
			id: input_color_preview_id,
			class: 'color-preview',
			style: 'background:' + input_color_value
		});
		createElementTag(div, 'button', {
			id: input_color_preview_id,
			class: 'color-random',
			onclick: 'generateRandomColor(' + input_count + ');',
			value: input_color_value
		}, 'Pick Random');

		createElementTag(div, 'input', {
			id: input_color_id,
			type: 'text',
			onchange: 'previewColor(this);',
			value: input_color_value
		});

		//column column value
		var td = createElementTag(tr, 'td', undefined, 'Value :');
		createElementTag(td, 'input', {
			id: input_value_id,
			type: 'text',
			on: 'return validateNumber(this);',
			value: input_value_value
		});
	}

	/**
	 * Delete selected row
	 * @param  {object} element to be deleted
	 */
	function deleteData(element) {
		var id = element.parentNode.parentNode.id;
		inputForm.removeChild(document.getElementById(id));
	}

	/**
	 * Fetch all data from form
	 */
	function fetchData() {
		var listdata = inputForm.rows;
		var is_error = false;
		var data_id = 0;

		//reset chartData
		chartData = {};
		//reset errorMessage
		resetErrorMsg();

		for (var i = 0; i < listdata.length; i++) {
			data_id = listdata[i].id.substring(9);
			chartData[i] = {
				name: document.getElementById('inputName' + data_id).value,
				value: document.getElementById('inputValue' + data_id).value,
				icon: document.getElementById('inputIcon' + data_id).value,
				color: document.getElementById('inputColor' + data_id).value
			};
			if (!validateIsNumber(document.getElementById('inputValue' + data_id).value)) {
				is_error = true;
				addErrorMsg('Value of Data ' + data_id + ' must be numeric.');
			}
		}
		if (!is_error) {
			hideErrorMsg();
			return true;
		}
		showErrorMsg();
		return false;
	}

	/**
	 * Reset error message object
	 */
	function resetErrorMsg() {
		dataErrorMsg = "";
		dataError.innerHTML = "";
	}

	/**
	 * Add Error Message
	 * @param {string} msg error message to displayed
	 */
	function addErrorMsg(msg) {
		var line = document.createElement('div');
		line.innerHTML = msg;
		dataError.appendChild(line);
	}

	/**
	 * Show Error Message
	 */
	function showErrorMsg() {
		dataError.setAttribute('style', 'display:block');
	}

	/**
	 * Hide Error Message
	 */
	function hideErrorMsg() {
		dataError.setAttribute('style', 'display:none');
	}

	/**
	 * Calculate Data total and percentage
	 */
	function calculateData() {
		var percentage = 0;
		totalData = 0;
		highestData = 0;
		//Calculate Total of Chart Value
		for (var i = 0; i < Object.size(chartData); i++) {
			totalData += parseInt(chartData[i]['value']);
			if (highestData < parseInt(chartData[i]['value'])) {
				highestData = parseInt(chartData[i]['value']);
			}
		}
		//Calculate Percentage of Chart Value
		for (var i = 0; i < Object.size(chartData); i++) {
			percentage = (chartData[i]['value'] / totalData) * 100;
			chartData[i]['percentage'] = percentage.toFixed(2);
		}
	}

	/* LIST OF FUNCTIONS FOR INITIALIZE CHART */
	var oriCanvasWidth = canvasWidth,
		oriCanvasHeight = canvasHeight;

	/**
	 * Generate Chart Based on Type Pie or Line
	 * @param  {string} type pie or line
	 * @return {boolean}      false on error
	 */
	function generateChart(type) {
		var is_error = false;

		//Fetch and check the data input
		if (!fetchData()) {
			return false;
		}

		//check input options
		if (type === 'Pie') {
			if (!validateIsNumber(inputChartA.value)) {
				is_error = true;
				addErrorMsg('Value of Circle Size must be numeric.');
			} else if (inputChartA.value < 100 || inputChartA.value > 180) {
				is_error = true;
				addErrorMsg('Value of Circle Size must be between 100 and 180.');
			}
		} else if (type === 'Line') {
			if (!validateIsNumber(inputHighestValue.value)) {
				is_error = true;
				addErrorMsg('Value of Max Line Chart must be numeric.');
			}
		}

		if (is_error) {
			showErrorMsg();
			return false;
		}
		hideErrorMsg();

		//Create canvas of chart
		//Save the original width and height of canvas
		canvasWidth = oriCanvasWidth;
		canvasHeight = oriCanvasHeight;
		chartCanvas = createElementTag(undefined, 'svg', {
			id: 'chartCanvas' + chartId,
			class: 'chart-canvas',
			width: canvasWidth,
			height: canvasHeight
		}, undefined, "http://www.w3.org/2000/svg");

		chartWrapper.insertBefore(chartCanvas, chartWrapper.firstChild);

		//Calculate data total and percentage
		calculateData();
		//Set the chart type to generate
		if (type === 'Pie') {
			generatePieChart();
		} else if (type === 'Line') {
			generateLineChart();
		}

		//Generate title and statistic of chart
		generateStatistic();

		chartId++;
	}


	/**
	 * Delete selected chart
	 * @param  {object} element chart Element
	 */
	function deleteChart(element) {
		var canvas = element.parentNode;
		canvas.parentNode.removeChild(canvas);
	}

	var chartTrTrX = {},
		chartTrTrY = {},
		chartTrScale = {};

	/**
	 * Transform Chart based on group
	 * @param  {object} groupObject group Object to be transformed
	 * @return {string}             transformation method
	 */
	function transformChart(groupObject) {
		console.log(groupObject.id);
		return 'translate(' + chartTrTrX[groupObject.id] + ',' + chartTrTrY[groupObject.id] + '),scale(' + chartTrScale[groupObject.id] + ')';
	}

	/**
	 * Set chart event listener to make it draggable
	 * @param  {object} groupObject group Object to be transformed
	 */
	function dragChart(groupObject) {
		var x = 0,
			y = 0,
			x2 = 0,
			y2 = 0;

		groupObject.addEventListener('mousedown', function(start) {
			x = start.x;
			y = start.y;
			this.onmousemove = function(move) {
				chartTrTrX[groupObject.id] = (x2 + (move.x - x));
				chartTrTrY[groupObject.id] = (y2 + (move.y - y));
				this.setAttribute('transform', transformChart(groupObject));
			};
		});
		groupObject.addEventListener('mouseup', function(end) {
			x2 = x2 + end.x - x;
			y2 = y2 + end.y - y;
			this.onmousemove = null;
		});
		groupObject.addEventListener('mouseout', function() {
			this.onmousemove = null;
		});
	}

	/**
	 * Create button for resizing and set event listener
	 * @param  {object} groupObject group Object to be transformed
	 */
	function resizeChart(groupObject) {
		var x = 0,
			y = 0,
			x2 = 0,
			y2 = 0,
			range = 0,
			currentScale = 1;

		var resize = createElementTag(groupObject.parentNode, 'image', {
			width: 100,
			height: 30,
			stroke: 'black',
			'stroke-width': 2,
			x: resizeX,
			y: resizeY
		}, undefined, "http://www.w3.org/2000/svg");

		resize.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', 'resize.svg');

		resize.addEventListener('mousedown', function(start) {
			x = start.x;
			this.onmousemove = function(move) {
				range = currentScale + ((move.x - start.x) / 1000);
				chartTrScale[groupObject.id] = range;
				groupObject.setAttribute('transform', transformChart(groupObject));
				currentScale = range;
			};
		});
		resize.addEventListener('mouseup', function(end) {
			x2 = x2 + end.x - x;
			y2 = y2 + end.y - y;
			this.onmousemove = null;
		});
	}

	/* END OF FUNCTIONS FOR INITIALIZE CHART */

	/* LIST OF FUNCTIONS FOR GENERATE LINE CHART */

	/**
	 * Set Line chart option value
	 */
	function initializeLineChartOption() {
		inputHighestValue.value = lineHighestValue;
	}

	/**
	 * Generate line chart
	 */
	function generateLineChart() {
		var line, icon, iconX, value, width, nextY = lineFromY,
			maxValue, maxY;

		//Create line group
		var newLineGroup = document.createElementNS("http://www.w3.org/2000/svg", 'g');
		newLineGroup.id = "chartGroup" + chartId;

		//Repetition of data to generate line
		for (var i = 0; i < Object.size(chartData); i++) {
			//Set line break
			nextY += 50;

			//Set max value
			if (inputHighestValue.value < highestData) {
				maxValue = highestData;
			} else {
				maxValue = inputHighestValue.value;
			}

			//If maxvalue below 10, show line chart with icons
			if (maxValue <= 10) {
				iconX = lineFromX + 1;
				//Do the repetition of generating icons by data value
				for (var o = 1; o <= chartData[i]['value']; o++) {
					//Check if the icon want to be shown with random color or not
					var setRandomColor = inputRandomColor.checked ? validateColor('random') : chartData[i]['color'];
					var d = icons[chartData[i]['icon']] + " M" + (iconX) + "," + nextY
					var transform = 'translate(' + (iconX) + ',' + (nextY) + '),scale(0.05)';

					var line = createElementTag(newLineGroup, 'path', {
						fill: setRandomColor,
						d: d,
						transform: transform,
					}, undefined, "http://www.w3.org/2000/svg");

					iconX += 42;
				}
			}
			//Else, we show the line chart with rectangle
			else {
				//Get line width
				width = chartData[i]['value'] * (lineWidth / maxValue);
				//Create line chart
				var line = createElementTag(newLineGroup, 'rect', {
					fill: chartData[i]['color'],
					x: lineFromX + 1,
					y: nextY,
					width: width,
					height: 30
				}, undefined, "http://www.w3.org/2000/svg");

				//Create icon beside of chart
				var d = icons[chartData[i]['icon']] + " M" + (lineFromX + width) + "," + nextY;
				var transform = 'translate(' + (lineFromX + width) + ',' + (nextY) + '),scale(0.05)';

				icon = createElementTag(newLineGroup, 'path', {
					fill: chartData[i]['color'],
					d: d,
					transform: transform,
				}, undefined, "http://www.w3.org/2000/svg");

			}

			//Put the data value beside line
			createElementTag(newLineGroup, 'text', {
				x: (lineFromX - 30),
				y: (nextY + 15),
				fill: 'black',
			}, chartData[i]['value'], 'http://www.w3.org/2000/svg');

		}

		//If the height of data is more than vertical line, then extend the line
		if ((nextY + 50) > (lineFromY + lineHeight)) {
			maxY = lineFromY + nextY;
		} else {
			maxY = lineFromY + lineHeight;
		}

		//If the height of data is more than canvas height, extend the canvas height
		if ((maxY) > canvasHeight) {
			canvasHeight = maxY;
			//Set width and height of chart
			chartCanvas.setAttribute('width', canvasWidth);
			chartCanvas.setAttribute('height', canvasHeight);
		}

		//Prepare horizontal line of chart
		createElementTag(newLineGroup, 'line', {
			stroke: 'black',
			'stroke-width': 3,
			'x1': lineFromX,
			'x2': lineFromX + lineWidth,
			'y1': maxY,
			'y2': maxY
		}, undefined, "http://www.w3.org/2000/svg");


		//Prepare vertical line of chart
		createElementTag(newLineGroup, 'line', {
			stroke: 'black',
			'stroke-width': 3,
			'x1': lineFromX,
			'x2': lineFromX,
			'y1': lineFromY,
			'y2': maxY
		}, undefined, "http://www.w3.org/2000/svg");

		//Prepare horizontal line value of chart
		createElementTag(newLineGroup, 'text', {
			x: lineFromX,
			y: (maxY + 20),
		}, '0', 'http://www.w3.org/2000/svg');

		createElementTag(newLineGroup, 'text', {
			x: (lineFromX + lineWidth) / 2,
			y: (maxY + 20),
		}, String(Math.round(maxValue / 2)), 'http://www.w3.org/2000/svg');

		createElementTag(newLineGroup, 'text', {
			x: (lineFromX + lineWidth),
			y: (maxY + 20),
		}, maxValue, 'http://www.w3.org/2000/svg');

		//Append to canvas
		chartCanvas.appendChild(newLineGroup);

		chartTrTrX[newLineGroup.id] = 0;
		chartTrTrY[newLineGroup.id] = 0;
		chartTrScale[newLineGroup.id] = 1;
		//Enable chart to be draggable
		dragChart(newLineGroup);
		//Enable chart to be resizable
		resizeChart(newLineGroup);
	}
	/* END OF FUNCTIONS FOR GENERATE LINE CHART */

	/* LIST OF FUNCTIONS FOR GENERATE PIE CHART */

	/**
	 * Set Pie Chart option value
	 */
	function initializePieChartOption() {
		inputChartA.value = chartA;
	}

	/**
	 * Generate pie chart
	 */
	function generatePieChart() {

		var start = 0;
		var finish = 0;
		var newPieGroup = document.createElementNS("http://www.w3.org/2000/svg", 'g');
		newPieGroup.id = "chartGroup" + chartId;

		//Generate pie chart by doing repetition of data
		for (var i = 0; i < Object.size(chartData); i++) {

			//Set the finish degree of current data to be used by next data
			finish = ((chartData[i]['value'] / totalData) * 360) + start;
			//Create the pie!
			newPieGroup.appendChild(createPieChart(start, finish, i, chartData[i]));
			//Set start degree of next data
			start = finish;
		}

		//Append to canvas
		chartCanvas.appendChild(newPieGroup);

		chartTrTrX[newPieGroup.id] = 0;
		chartTrTrY[newPieGroup.id] = 0;
		chartTrScale[newPieGroup.id] = 1;
		//Enable chart to be draggable
		dragChart(newPieGroup);
		//Enable chart to be resizable
		resizeChart(newPieGroup);

	}

	/**
	 * Create Pie chart
	 * @param  {integer} angleFrom [description]
	 * @param  {integer} angleTo   [description]
	 * @param  {string} uniqueID  [description]
	 * @param  {object} data      [description]
	 * @return {object}           Pie chart object
	 */
	function createPieChart(angleFrom, angleTo, uniqueID, data) {

		//Get Value from option
		chartA = inputChartA.value;

		//Get different angle for pie size
		var angleDiff = Math.abs(angleFrom - angleTo);
		var from = chartA / 180 * angleFrom;
		var to = chartA / 180 * angleTo;

		//If the different is bigger than 180deg, the chart arc must set to 1
		if (angleDiff <= 180) {
			chartArcFlag = 0;
		} else {
			chartArcFlag = 1;
		}

		//If the difference is 360, create full circle
		//old solution had a bug using svg path
		if (angleDiff === 360) {
			var chartType = 'circle';
			var chartAttributes = {
				cx: chartFromX,
				cy: chartFromY,
				r: chartA
			};
		} else {
			//Set the coordinates and path d
			//Formula inspired from : http://jbkflex.wordpress.com/2011/07/28/creating-a-svg-pie-chart-html5/
			x1 = chartFromX + chartA * Math.cos(Math.PI * from / chartA);
			y1 = chartFromY + chartA * Math.sin(Math.PI * from / chartA);
			x2 = chartFromX + chartA * Math.cos(Math.PI * to / chartA);
			y2 = chartFromY + chartA * Math.sin(Math.PI * to / chartA);
			chartD = "M" + chartFromX + "," + chartFromY + " L" + x1 + "," + y1 + " A" + chartA + "," + chartA + " 0 " + chartArcFlag + ",1 " + x2 + "," + y2 + " z";

			var chartType = 'path';
			var chartAttributes = {
				d: chartD
			};
		}
		chartAttributes.id = "pieChart" + uniqueID;
		var newPieChart = createElementTag(undefined, chartType, chartAttributes, undefined, 'http://www.w3.org/2000/svg');

		newPieChart.style.stroke = "#000"; //Set stroke colour
		newPieChart.style.strokeWidth = "0px"; //Set stroke width
		newPieChart.style.fill = data['color'];

		return newPieChart;
	}
	/* END OF FUNCTIONS FOR GENERATE PIE CHART */

	/* LIST OF FUNCTIONS FOR GENERATE STATISTIC */

	/**
	 * Create the title, statistic, and close button of chart
	 */
	function generateStatistic() {

		//Insert the title from input form
		createElementTag(chartCanvas, 'text', {
			'font-size': 36,
			x: titleX,
			y: titleY
		}, inputChartTitle.value, 'http://www.w3.org/2000/svg');

		//Create delete button
		var chartDelete = createElementTag(chartCanvas, 'image', {
			x: deleteX,
			y: deleteY,
			width: 30,
			height: 30,
			class: 'chart-button',
			onclick: 'deleteChart(this);'
		}, undefined, 'http://www.w3.org/2000/svg');

		chartDelete.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', 'delete.svg');

		//Set position of data statistic
		var posX = statisticX,
			posY = statisticY;

		var statistic = document.createElementNS("http://www.w3.org/2000/svg", 'g');

		//Statistic title
		createElementTag(statistic, 'text', {
			x: statisticX,
			y: statisticY,
			'font-size': 24,
		}, 'Data Statistic', 'http://www.w3.org/2000/svg');

		//Lower the statistic position
		posY += 20;

		//Do the repetition of data
		for (var i = 0; i < Object.size(chartData); i++) {

			//Show icon of data with selected color
			var d = icons[chartData[i]['icon']] + " M" + posX + "," + posY;
			var transform = 'translate(' + posX + ',' + (posY - 20) + '),scale(0.05)'

			createElementTag(statistic, 'path', {
				d: d,
				fill: chartData[i]['color'],
				transform: transform,
			}, undefined, 'http://www.w3.org/2000/svg');

			//Show data value
			var textContent = chartData[i]['name'] + ' : ' + chartData[i]['value'].toLocaleString() + ' (' + chartData[i]['percentage'] + '%)';

			createElementTag(statistic, 'text', {
				x: Number(posX + 28),
				y: posY,
				fill: chartData[i]['color'],
			}, textContent, 'http://www.w3.org/2000/svg');

			//If the statistic height is bigger than canvas, extend the canvas
			if ((posY + 20) > canvasHeight) {
				canvasHeight += statLineHeight;
				//Set width and height of chart
				chartCanvas.setAttribute('width', canvasWidth);
				chartCanvas.setAttribute('height', canvasHeight);
			}
			posY += statLineHeight;
		}

		chartCanvas.appendChild(statistic);
	}
	/* END OF FUNCTIONS FOR GENERATE STATISTIC */

	/* LIST OF FUNCTIONS FOR HELPER */

	/**
	 * add HTML Element
	 * @param {object} tagParent     html tag parent node
	 * @param {string} tagName       tag to be created
	 * @param {object} tagAttributes all tag attributes
	 * @param {string} tagContent    content of html tag
	 * @optional {string} nameSpace     name space of tag
	 */
	function createElementTag(tagParent, tagName, tagAttributes, tagContent, nameSpace) {
		if (typeof(nameSpace) == "string") {
			var element = document.createElementNS(nameSpace, tagName);
		} else {
			var element = document.createElement(tagName);
		}
		if (typeof(tagAttributes) == 'object') {
			for (var key in tagAttributes) {
				if (tagAttributes.hasOwnProperty(key)) {
					element.setAttribute(key, tagAttributes[key]);
				}
			}
		}
		if (typeof(tagContent) == "string") {
			element.textContent = tagContent;
		}
		if (typeof(tagParent) == "object") {
			tagParent.appendChild(element);
		}

		return element;
	}


	/**
	 * Validate color if random color is requested
	 * @param  {string} val
	 * @return {string}     hex color
	 */
	function validateColor(val) {
		if (val === 'random') {
			return getRandomColor();
		}
		return val;
	}

	/**
	 * Return random hex color
	 * @return {string} random hex color
	 */
	function getRandomColor() {
		return '#' + Math.floor(Math.random() * 16777215).toString(16).toUpperCase();
	}

	/**
	 * Preview color selected
	 * @param  {object} element element to receive preview
	 */
	function previewColor(element) {
		var id = element.id.substring(10, 11);
		var color_preview = document.getElementById('inputColorPreview' + id);
		color_preview.setAttribute('style', "background:" + element.value);
	}

	/**
	 * Function to set random color value to the input
	 * @param  {string} colorId
	 */
	function generateRandomColor(colorId) {
		var random_color = getRandomColor();
		var color_input = document.getElementById('inputColor' + colorId);
		color_input.value = random_color;
		previewColor(color_input);
	}

	/**
	 * Validate passed value is a number
	 * @param  {any} value value to be tested
	 * @return {boolean}       true if number
	 */
	function validateIsNumber(value) {
		return !isNaN(value);
	}
	/* END OF FUNCTIONS FOR HELPER */

	return publicFunctions;
})();

//Exposed global functions to leave HTML file alone!
var addData = chartGenerator.addData;
var generateChart = chartGenerator.generateChart;
var deleteData = chartGenerator.deleteData;
var deleteChart = chartGenerator.deleteChart;
var generateRandomColor = chartGenerator.generateRandomColor;
