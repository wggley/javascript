# Summary
Some Vanilla JS snippets: 

* Pie and Line Chart Creator. 

* Modal with Overlay Library based on Firefox alert/confirm/prompt . 

* A function that based on an array it calculates unique elements and how many they appears. 

## Getting Started
Just download the code and open in a browser the index.html file.

## Author
wggley:

* [Linkedin](https://www.linkedin.com/pub/wallace-goulart-gaudie-ley/7/66b/23b)

* [Github](https://github.com/wggley)

* [Bitbucket](https://bitbucket.org/wggley)