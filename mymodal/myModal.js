/**
 * My Modal API
 * @author Wallace Goulart Gaudie Ley
 * @version 1.0.0
 * @description Simple Vanilla Javascript Modal with overlay based on Firefox Alert/Confirm/Prompt Modal
 * It Returns a Javascript Promise from buttons clicked on Modal
 * @namespace myModal
 *
 * @param           {object}       config          My Modal Configuration boject
 * @attribute       {string}       mode            Modal Mode behaviour execution
 *                  @description                   @default 'alert' values: 'confirm', 'prompt', 'custom'
 * @attribute       {string}       title           Modal Title to be displayed
 *                  @description                   @default 'my custom popup'
 * @attribute       @optional {string}             description     Modal custom description
 *                  @description                   @default 'my custom description for popup'
 * @attribute       {object}       html            HTML element object
 *                  @description                   only used in 'custom' mode
 * @attribute       {boolean}      okButton        shows Ok Button
 *                  @description                   only used in 'custom' mode
 * @attribute       {function}     @optional       okCallback      Ok Button Callback (called before returns Promise)
 *                  @description                   only used in 'custom' mode
 * @attribute       {boolean}      cancelButton    shows Cancel Button
 *                  @description                   only used in 'custom' mode
 * @attribute       {function }    @optional       cancelCallback  Cancel Button Callback (called before returns Promise)
 *                  @description                   only used in 'custom' mode
 *
 * @return {Promise} returns a Promise to callee
 *
 * @example
 *     var config = {
 *           mode: 'alert',
 *           title: 'my custom popup',
 *           description: 'my custom description for popup',
 *           html: myCustomHtmlObject,
 *           okButton: true,
 *           okCallback: myOkFunction,
 *           cancelButton: true,
 *           cancelCallback: myCancelFunction,
 *     };
 *
 *     myModal.show({config});
 *
 *     //with Promise
 *     myModal.show({config}).then(function(){
 *         //do something....
 *     });
 */

var myModal = myModal || (function() {
    // Class properties
    var modalConfig;
    var stackModals = [];
    var isModalVisible = false;
    var myModalHTML = '<div class="my-modal-overlay"></div>';
    myModalHTML += '<section class="my-modal">';
    myModalHTML += '    <header class="my-modal-title">';
    myModalHTML += '        <h4 class="my-modal-title-text"></h4>';
    myModalHTML += '    </header>';
    myModalHTML += '    <div class="my-modal-description">';
    myModalHTML += '    </div>';
    myModalHTML += '    <input type="text" class="my-modal-input" placeholder="type text here"/>';
    myModalHTML += '    <footer class="my-modal-buttons-area">';
    myModalHTML += '        <button class="my-modal-button">Cancel</button>';
    myModalHTML += '        <button class="my-modal-button">OK</button>';
    myModalHTML += '    </footer>';
    myModalHTML += '</section>';


    var publicMethods = {
        'show': show,
        'hide': hide
    };

    /**
     * Show Modal window based on configuration
     * @param  {object}  My Modal config properties
     * @see comments above
     * @return {Promise} returns a Promise to callee
     */
    function show(config, doStack) {
        doStack = typeof(doStack) == "undefined" ? true : doStack;

        config = configModal(config);

        if (doStack) {
            stackModals.push(function() {
                show(config, false);
            });
        }

        if (isModalVisible) {
            hide();
        }

        createAndConfigureHTMLElements(config);

        isModalVisible = true;

        return promise(config);
    }

    /**
     * Configure Modal Behaviour
     * @param  {object} config @see my Modal comments above
     * @return {object} config @see my Modal comments above
     */
    function configModal(config) {
        config = config || {};
        modalConfig = config;
        config.mode = config.mode || 'alert';
        config.title = typeof(config.title) == "undefined" ? 'my custom popup' : config.title;
        config.description = typeof(config.description) == "undefined" ? 'my custom description for popup' : config.description;
        config.html = typeof(config.html) == "undefined" ? undefined : config.html;

        if (config.mode == "custom") {
            config.okButton = typeof(config.okButton) == "boolean" ? config.okButton : true;
            if (typeof(config.okCallback) != "function") {
                config.okCallback = function(response) {
                    return response;
                };
            }

            config.cancelButton = typeof(config.cancelButton) == "boolean" ? config.cancelButton : true;

            if (typeof(config.cancelCallback) != "function") {
                config.cancelCallback = function(response) {
                    return response;
                };
            }
        } else {
            config.okCallback = function(response) {
                return response;
            };

            config.cancelCallback = function(response) {
                return response;
            };
        }

        return config;
    }

    /**
     * Create HTML Elements and Configure based on config object
     * @param  {object} config @see my Modal comments above
     */
    function createAndConfigureHTMLElements(config) {
        var fragment = document.createDocumentFragment();
        var temp = document.createElement('div');
        temp.innerHTML = myModalHTML;
        while (temp.firstChild) {
            fragment.appendChild(temp.firstChild);
        }
        document.body.insertBefore(fragment, document.body.childNodes[0]);
        document.getElementsByClassName('my-modal-title-text')[0].innerHTML = config.title;
        document.getElementsByClassName('my-modal-description')[0].innerHTML = config.description;
        if (config.mode == 'alert') {
            document.getElementsByClassName("my-modal-button")[0].classList.add('my-modal-hide');
        } else if (config.mode == "custom") {
            if (!config.cancelButton) {
                document.getElementsByClassName("my-modal-button")[0].classList.add('my-modal-hide');
            }
            if (!config.okButton) {
                document.getElementsByClassName("my-modal-button")[1].classList.add('my-modal-hide');
            }
            if (typeof(config.html) != "undefined") {
                document.getElementsByClassName("my-modal-description")[0].appendChild(config.html);
            }
        }
        if (config.mode != "prompt") {
            document.getElementsByClassName("my-modal-input")[0].classList.add('my-modal-hide');
        }
    }

    /**
     * Create Promise based on cofig object
     * @param  {object} config @see my Modal comments above
     */
    function promise(config) {
        var myModalPromise = new Promise(
            function(resolve, reject) {
                document.getElementsByClassName('my-modal-button')[0].onclick = function() {
                    resolve(config.cancelCallback(cancelButtonAction()));
                    //resolve(config.cancelCallback.apply(this, arguments));
                };
                document.getElementsByClassName('my-modal-button')[1].onclick = function() {
                    resolve(config.okCallback(okButtonAction()));
                    //resolve(config.okCallback.apply(this, arguments));
                };
            });

        myModalPromise.then(
            function(val) {
                return val;
            });

        myModalPromise.catch(
            function(reason) {
                console.log('Handle rejected promise (' + reason + ') here.');
                return reason;
            });
        return myModalPromise
    }


    /**
     * Hide current Modal
     */
    function hide() {
        var element = document.getElementsByClassName('my-modal-overlay')[0];
        element.parentNode.removeChild(element);
        var element = document.getElementsByClassName('my-modal')[0];
        element.parentNode.removeChild(element);
        isModalVisible = false;
    }

    /**
     * Default cancel button action
     */
    function cancelButtonAction() {
        hide();
        validateStackModals();
        return false;
    }

    /**
     * Default ok button action
     */
    function okButtonAction() {
        var promptValue;
        if (modalConfig.mode == "prompt") {
            promptValue = document.getElementsByClassName("my-modal-input")[0].value;
        }
        hide();
        validateStackModals();
        if (typeof(promptValue) != "undefined") {
            return promptValue;
        }
        return true;
    }

    /**
     * Validate if needs to show a popup on stack
     */
    function validateStackModals() {
        stackModals.pop();
        if (stackModals.length > 0) {
            stackModals[stackModals.length - 1]();
        }
    }

    return publicMethods;
})();
