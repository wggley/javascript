/**
 * Main script anonymous function
 */
var main = main || (function() {
    /**
     * Run function test
     */
    function run(option) {
        var config = {};
        switch (option) {
            case 1:
                config = {
                    title: '',
                    description: 'test alert modal'
                };

                break;
            case 2:
                config = {
                    mode: 'confirm',
                    title: 'confirm modal',
                    description: 'test confirm modal',
                };
                break;
            case 3:
                config = {
                    mode: 'prompt',
                    title: 'prompt modal',
                    description: 'test prompt modal',
                }
                break;
            case 4:
                var html = document.createElement('div');
                html.innerHTML = "test <b>Bold<b>";
                config = {
                    mode: 'custom',
                    title: 'custom modal',
                    description: 'test custom modal',
                    html: html,
                    okCallback: function(response) {
                        alert('okCallback execution response : ' + response);
                        return response;
                    },
                    cancelCallback: function(response) {
                        alert('cancelCallback execution response : ' + response);
                        return response;
                    }
                }
                break;
            case 5:
                //stack behaviour
                run(1);
                run(2);
                run(3);
                run(4);
                break;
            default:

                break;

        }
        // returns a promise
        myModal.show(config).then(promise);
    }

    /**
     * Threats Promise response
     * @param  {mixed} response boolean or string
     */
    function promise(response) {
        document.getElementById('response').innerHTML = JSON.stringify(response);
    }

    return {
        'run': run
    };
})();
