/**
 * Main script anonymous function
 */
var main = main || (function() {
    /**
     * Count unique elements occurrences in an array and subarrays(if it's the case)
     * @param  {Array} myArray array to be tested
     * @return {object}         array with unique occurrences of elements
     */
    function countElementsOccurrences(myArray) {
        var result = {};
        for (var index = 0; index < myArray.length; index++) {
            if (myArray[index].constructor === Array) {
                //sub arrays
                var recursive = countElementsOccurrences(myArray[index]);
                //merge result with recursive result
                for (var attrname in recursive) {
                    if (attrname in result) {
                        result[attrname] += recursive[attrname];
                    } else {
                        result[attrname] = recursive[attrname];
                    }
                }
            } else {
                if (myArray[index] in result) {
                    result[myArray[index]]++;
                } else {
                    result[myArray[index]] = 1;
                }
            }
        };
        return result;
    }

    /**
     * Run function test
     */
    function run() {
        try {
            var start = new Date().getTime();

            var myTestArray = JSON.parse(document.getElementById('test').value.replace(/\'/g, '"'));
            var result = countElementsOccurrences(myTestArray);

            var end = new Date().getTime();
            var time = end - start;
            console.log('Execution time: ' + time + ' milliseconds');
            console.log(result);

            document.getElementById('result').innerHTML = JSON.stringify(result, null, 4);
        } catch (e) {
            document.getElementById('result').innerHTML = 'error evaluating input';
        }
    }

    return {
        'run': run
    };
})();
